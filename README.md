# Measuring Organizational Resilience

This project contains data and the code to showcase different methods of measuring organizational resilience when it is conceptualized as a performance outcome.

## Downloading the data

The data is downloaded from [WRDS](https://wrds-www.wharton.upenn.edu/) and requires an account accordingly.

**RawDataDownload.ipynb** downloads and saves two datasets, **CompustatQuarterly.csv** and the **GICSbyGV.csv**. The former is the quarterly Compustat data, and the latter the GICS industry codes per company, each identified by their gvkey. 

## Processing the data

**Data_Processing.ipynb** imports both of these datasets, matches the company financials data with GICS data through company gvkeys. Drops companies that appear under more than 2 six-digit GICS codes in the given time frame. 

For the particular application presented in the article, we chose GICS industry code 151020 and considered only those companies that had consistent data between 2007 and 2020 as well as more than 5 datapoints. We finally order all the organizations by their market share and saved them into the dataset **Industry_Data_Processed.csv**.

## Analyzing the data

**Industry_Data_Processed.csv** serves as the basis for further analysis and shared in the repository for replication purposes.

1) **Absolute_Resilience_Forecasting.ipynb**: Implements MMM's resilience measure with respect to forecasting counterfactual
2) **Relative_Resilience_Peers.ipynb**: Implements MMM and its peers' relative resilience with respect to peer average
3) **Relative_Resilience_Industry.ipynb**: Implements MMM and its peers' relative resilience with respect to industry average
4) **Absolute_Relative_Comparison.ipynb**: Combines item 1 and 2 into one file to create Figure 2a top and bottom together.
